package io.fgame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import io.fgame.base.*;
import io.fgame.base.tiles.CityTile;
import io.fgame.base.tiles.TerrainTile;
import io.fgame.base.tiles.TerrainType;
import io.fgame.base.ui.UIManager;

public class Fracker implements Screen {
    float MAX_ZOOM = 1.578593f;
    private Rectangle r1;
    private SpriteBatch batch;
    private TerrainTile[][] sprites;
    private CityTile[][] cityTiles;
    private OrthographicCamera camera;
    private CameraController controller;
    private Sprite mHouse;
    private InputMultiplexer inputMultiplexer;
    private UIManager uim;
    private GestureDetector gestureDetector;
    private GameObjectType gameObjectType;
    private boolean placementMode;

    private GameObject tempObject;
    private boolean placeable;
    private GameObjectManager gameObjectManager;

    public Fracker() {

    }

    @Override
    public void show() {
        uim = new UIManager(Gdx.files.internal("uiskin.json"));



        //uim.createPurchaseDialog();
        uim.createMasterMenu();

        inputMultiplexer = new InputMultiplexer();

        batch = new SpriteBatch();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.zoom = 0.02f;
        controller = new CameraController();
        gestureDetector = new GestureDetector(20, 0.5f, 2, 0.15f, controller);

        sprites = new TerrainTile[128][128];
        cityTiles = new CityTile[128][128];
        sprites = GameState.INSTANCE.sprites;
        //MapGenerator.generateTextures(sprites);
        cityTiles = GameState.INSTANCE.ct;

        r1 = new Rectangle().set(camera.frustum.planePoints[0].x, camera.frustum.planePoints[0].y, (Gdx.graphics.getWidth() + 5), (Gdx.graphics.getHeight() + 5));

        inputMultiplexer.addProcessor(gestureDetector);
        inputMultiplexer.addProcessor(uim.getStage());
        Gdx.input.setInputProcessor(inputMultiplexer);


        gameObjectManager = new GameObjectManager();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        controller.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        //Only attempt to draw sprites that would be visible in the frustum anyway (they are aligned with the coord system)
        int minX = (int) camera.frustum.planePoints[0].x;
        int maxX = (int) camera.frustum.planePoints[1].x + 2;
        int minY = (int) camera.frustum.planePoints[0].y;
        int maxY = (int) camera.frustum.planePoints[3].y + 2;

        if (minX > 0 && minY > 0 && maxX < 128 && maxY < 128) {
            for (int x = minX; x < maxX; x++) {
                for (int y = minY; y < maxY; y++) {
                    if (sprites[x][y].getBoundingRectangle().overlaps(r1)) {
                        //Draw, if off-screen it will be culled.
                        sprites[x][y].draw(batch);

                        if (sprites[x][y].getOilLevel() != 0) {
                            sprites[x][y].setColor(0, 0, 0, sprites[x][y].getOilLevel() / 1.9f);
                        }
                        if (cityTiles[x][y] != null) {
                            cityTiles[x][y].draw(batch);
                        }
                    }
                }
            }
        } else {
            //This needs to be removed.
            for (int x = 0; x < 128; x++) {
                for (int y = 0; y < 128; y++) {
                    if (camera.frustum.pointInFrustum(x, y, 0)) {
                        sprites[x][y].draw(batch);

                        if (sprites[x][y].getOilLevel() != 0) {
                            sprites[x][y].setColor(0, 0, 0, sprites[x][y].getOilLevel() / 1.9f);
                        }
                        if  (cityTiles[x][y] != null){
                            cityTiles[x][y].draw(batch);
                        }
                    }
                }
            }
        }

        if (!uim.queue.isEmpty()) {
            gameObjectType = uim.queue.poll();
            placementMode = true;

        }
        if (placementMode && gameObjectType != null) {
            Vector3 worldPos = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            switch (gameObjectType) {
                case OILWELL:
                    if (worldPos.x > 0 && worldPos.x < 128 && worldPos.y > 0 && worldPos.y < 128) {
                        tempObject = new OilWell(new Texture("ow2.png"));
                        tempObject.setPosition(worldPos.x, worldPos.y);
                        tempObject.setSize(1,1);
                        //tempObject.setSize(GameObjectType.OILWELL.getWidth(), GameObjectType.OILWELL.getHeight());
                        tempObject.draw(batch);
                        gameObjectType = null;
                    }
                case FRWELL:
                    break;
                default:
                    break;
            }
        } else if (placementMode){
            Vector3 worldPos = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            int x = (int)worldPos.x;
            int y = (int)worldPos.y;
            if (sprites[x][y].getTileType() == TerrainType.DESERT){
                Gdx.app.log("FRGame", "Type desert.");
            }
            tempObject.setPosition((int)worldPos.x, (int)worldPos.y);
            tempObject.draw(batch);

            if (sprites[x][y].getTileType() != TerrainType.DESERT){
                tempObject.setColor(Color.RED);
                placeable = false;
            } else {
                tempObject.setColor(Color.WHITE);
                placeable = true;
            }
        }
        if (!placementMode){
           gameObjectManager.render(batch);
        }


        batch.end();
        uim.draw(delta);


    }

    @Override
    public void resize(int width, int height) {
        //stage.getViewport().update(width, height, true);
        uim.getStage().getViewport().update(width, height, true);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();

    }

    class CameraController implements GestureDetector.GestureListener {
        float velX, velY;
        boolean flinging = false;
        float initialScale = 5f;
        public boolean touchDown(float x, float y, int pointer, int button) {
            flinging = false;
            initialScale = camera.zoom;
            return false;
        }

        @Override
        public boolean tap(float x, float y, int count, int button) {
            Gdx.app.log("FRGame", "TAP AT: " + x + "," + y);
            if (count > 1 && placeable){
                //get the GameObject type
                Vector3 worldPos = camera.unproject(new Vector3(x,y,0));
                gameObjectManager.newGameObject(tempObject, (int) worldPos.x, (int) worldPos.y);
                placementMode = false;
            }

            return false;
        }

        @Override
        public boolean longPress(float x, float y) {
            return false;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {
            flinging = true;
            if (placementMode){
                velX = camera.zoom * velocityX * 0.1f;
                velY = camera.zoom * velocityY * 0.1f;
            }else{
                velX = camera.zoom * velocityX * 0.4f;
                velY = camera.zoom * velocityY * 0.4f;
            }
            return false;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            camera.position.add(-deltaX * camera.zoom, deltaY * camera.zoom, 0);
            return false;
        }

        @Override
        public boolean panStop(float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean zoom(float originalDistance, float currentDistance) {
            float ratio = originalDistance / currentDistance;
            if ((initialScale * ratio) < MAX_ZOOM) {
                camera.zoom = initialScale * ratio;
            }
            return false;
        }

        @Override
        public boolean pinch(Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
            return false;
        }

        public void update() {
            if (flinging) {
                velX *= 0.98f;
                velY *= 0.98f;
                camera.position.add(-velX * Gdx.graphics.getDeltaTime(), velY * Gdx.graphics.getDeltaTime(), 0);
                if (Math.abs(velX) < 0.01f) velX = 0;
                if (Math.abs(velY) < 0.01f) velY = 0;
            }
        }

    }
}
