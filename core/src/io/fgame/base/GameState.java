package io.fgame.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import io.fgame.base.tiles.CityTile;
import io.fgame.base.tiles.TerrainTile;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public enum GameState {
    INSTANCE;
    //the objects that need to be maintained across state
    public boolean firstRun;
    public List permGameObjects;
    public TerrainTile[][] sprites;
    public CityTile[][] ct;
    public String version;

    private File fbin;

    GameState() {
        version = "0.0 DEV\n";
        fbin = new File("file.bin");

        sprites = new TerrainTile[128][128];
        ct = new CityTile[128][128];
        permGameObjects = new LinkedList<GameObject>();

        Json json = new Json();

        if (fbin.exists()) {
            try {

                BufferedReader br = new BufferedReader(new FileReader(fbin));
                TerrainSaveObject[][] t;
                String line;
                int breakCount = 0;
                String fVersion = "";
                StringBuilder fTrObject = new StringBuilder();
                String fGameObject;
                while ((line = br.readLine()) != null) {

                    if (line.contains("/")) {
                        breakCount++;
                        Gdx.app.log("FRGame", "BC: " + breakCount);
                    } else if (breakCount == 1) {
                        fTrObject.append(line);
                    }
                }
                Gdx.app.log("FRGame", "L: " + fTrObject.toString() + " /");
                t = json.fromJson(TerrainSaveObject[][].class, fTrObject.toString());
                sprites = MapGenerator.fromTerrainSaveObject(t);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            firstRun = true;
            try {
                fbin.createNewFile();
                Gdx.app.log("FRGame", "created new file.zz");
            } catch (IOException e) {
                e.printStackTrace();
            }
            sprites = MapGenerator.generateMap(128, 128);
            ct = CityGenerator.createCities(sprites);

        }
    }


    public CityTile[][] getCityTile() {
        return ct;
    }

    public TerrainTile[][] getTerrainTile() {
        return sprites;
    }

    public List getGameObjects() {
        return permGameObjects;
    }


    public void autosave() throws IOException {
        Json json = new Json();
        TerrainSaveObject[][] ter = new TerrainSaveObject[128][128];
        if (fbin.exists()) {
            fbin.delete();
            fbin.createNewFile();
        } else {
            fbin.createNewFile();
        }

        //write to the terrain save objects.
        FileOutputStream fo = new FileOutputStream(fbin);
        for (int x = 0; x < 128; x++) {
            for (int y = 0; y < 128; y++) {
                ter[x][y] = new TerrainSaveObject();
                //terrain features
                ter[x][y].oilLevel = sprites[x][y].getOilLevel();
                ter[x][y].t = sprites[x][y].getTileType();
                //city features
                if (ct[x][y] != null) {
                    ter[x][y].cMasterTile = ct[x][y].isMasterTile();
                    ter[x][y].cMasterTileLocation = ct[x][y].getMTile();
                    if (ct[x][y].getCityName() != null) {
                        ter[x][y].cName = ct[x][y].getCityName();
                    }
                }
            }
        }

        fo.write("FRGAME\n".getBytes());
        fo.write("V: ".getBytes());
        fo.write(version.getBytes());
        fo.write("/frgame/\n".getBytes());
        fo.write(json.prettyPrint(ter).getBytes());
        fo.write("\n/frgame/\n".getBytes());

    }
}