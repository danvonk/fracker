package io.fgame.base;

import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dan on 17/07/14.
 */
public class Quadtree {
    private int MAX_OBJECTS = 10;
    private int MAX_LEVELS = 5;

    private int level;
    private List objects;
    private Rectangle bounds;
    private Quadtree[] nodes;

    public Quadtree(int lvl, Rectangle b) {
        level = lvl;
        objects = new ArrayList();
        bounds = b;
        nodes =  new Quadtree[4];
    }

    //recursively clear all nodes
    public void clear() {
        objects.clear();

        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] != null) {
                nodes[i].clear();
                nodes[i] = null;
            }
        }
    }
    //split the node into four sub-nodes by dividing the node into four equal parts.
    public void split() {
        int subWidth = (int)(bounds.getWidth() /2);
        int subHeight = (int)(bounds.getHeight() /2);
        int x = (int)bounds.getX();
        int y = (int)bounds.getY();

        nodes[0] = new Quadtree(level+1, new Rectangle(x+ subWidth, y, subWidth, subHeight));
        nodes[1] = new Quadtree(level+1, new Rectangle(x, y, subWidth, subHeight));
        nodes[2] = new Quadtree(level+1, new Rectangle(x, y + subHeight, subWidth, subHeight));
        nodes[3] = new Quadtree(level+1, new Rectangle(x + subWidth, y + subHeight, subWidth, subHeight));

    }

    private int getIndex(Rectangle rec) {
        int index = -1;
        double verticalMidpoint = bounds.getX() + (bounds.getWidth() /2);
        double horizontalMidpoint = bounds.getY() + (bounds.getHeight() /2);

        //object can completely fit within the top quadrants
        boolean topQuadrant = (rec.getY() < horizontalMidpoint && rec.getY() + rec.getHeight() < horizontalMidpoint);
        //object can completely fit within the bottom quadrants
        boolean bottomQuadrant = (rec.getY() > horizontalMidpoint);

        //object can completely git within the left quadrants
        if (rec.getX() < verticalMidpoint && rec.getX() + rec.getWidth() < verticalMidpoint) {
            if (topQuadrant) {
                index = 1;
            } else if (bottomQuadrant) {
                index = 2;
            }
        }
        //object can completely fit within the right quadrants
        return index;

    }
}
