package io.fgame.base;

import io.fgame.base.tiles.TerrainTile;


/**
 * Created by user for io.fgame.base at 20/07/14 12:28
 */
public class ResourceGenerator {
    private float minOil = 0.7f; //how large the perlin noise must be to create the oil.

    public ResourceGenerator() {

    }

    //TODO: Add in some other resources e.g. Natural Gas.
    public void createResources(TerrainTile[][] tMap) {
        float[][] baseNoise = PerlinNoise.generatePerlinNoise(128, 128, 6);
        for (int x = 0; x < 128; x++) {
            for (int y = 0; y < 128; y++) {
                if (baseNoise[x][y] > minOil) {
                    tMap[x][y].setOilLevel(baseNoise[x][y]);
                } else {
                    tMap[x][y].setOilLevel(0);
                }
            }
        }


    }
}
