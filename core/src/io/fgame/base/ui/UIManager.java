package io.fgame.base.ui;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import io.fgame.base.GameObjectType;
import io.fgame.base.GameState;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;


/**
 * Created by user for io.fgame.base at 25/07/14 23:13
 */
public class UIManager {
    private Skin skin;
    private Stage stage;
    private Table t;
    private Dialog dlg;
    private boolean isPlacementMode;
    public Queue<GameObjectType>queue;

    public Queue getQueue(){
        return queue;
    }

    public UIManager(FileHandle f){
        this.stage = new Stage(new ScreenViewport());
        skin = new Skin(f);

        t = new Table();
        t.setFillParent(true);
        t.bottom();
        t.row().padRight(30f).height(50);
        t.left();
        stage.addActor(t);

        queue = new LinkedList<GameObjectType>();

    }

    //Menu at the bottom which has buttons for terrain, game objects, money etc.
    public void createMasterMenu(){

        Label label = new Label("Money: $0", skin);
        label.setPosition(0,400);
        t.add(label);

        Button btn = new Button(skin);
        btn.setSize(40,40);
        btn.setPosition(20,0);
        btn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                createPurchaseDialog();

            }
        });
        t.add(btn);
    }

    public Stage getStage() {
        return stage;
    }

    public void createPurchaseDialog(){
        //TODO: Make this UI work.
        dlg = new Dialog("...", skin);
        dlg.setMovable(false);
        dlg.setPosition(0, 40);

        Table subT = new Table(skin);
        ScrollPane pane = new ScrollPane(subT, skin);
        pane.setFillParent(true);
        subT.setFillParent(true);
        dlg.add(pane);
        Button btn = new Button(skin);
        Button btn2 = new Button(skin);

        subT.row().height(200);
        subT.row().width(200);

        btn.add("11111111111111111111111");

        subT.row().height(200);
        subT.row().width(200);

        btn2.add("22222222222222222222222");
        btn2.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dlg.hide();
                setPlacementMode(true);
                queue.offer(GameObjectType.OILWELL);

            }
        });
        subT.add(btn);

        subT.row().height(200);
        subT.row().width(200);

        subT.add(btn2);

        subT.row().height(200);
        subT.row().width(200);

        Button btn3 = new Button(skin);
        btn3.add("33333333333333333333333");
        subT.add(btn3);

        subT.row().height(200);
        subT.row().width(200);

        Button btn4 = new Button(skin);
        btn4.add("44444444444444444444444");
        subT.add(btn4);

        stage.addActor(dlg);
        try {
            GameState.INSTANCE.autosave();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void draw(float delta) {
        stage.act(delta);
        stage.draw();

    }

    public boolean isPlacementMode() {
        return isPlacementMode;
    }

    public void setPlacementMode(boolean isPlacementMode) {
        this.isPlacementMode = isPlacementMode;
    }

}

/*
  Button button = new Button(skin);
        button.setSize(50,50);
        button.setPosition(100, 0);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                label.setText("$$$$$" + money++);
            }
        });
 */