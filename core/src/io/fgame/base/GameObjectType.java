package io.fgame.base;

/**
 * Created by user for io.fgame.base at 28/07/14 17:07
 */


//Will probably extend with params such as cost, public opinion etc.?
public enum GameObjectType
{
    //name, WIDTH, HEIGHT
    OILWELL (2,2),
    FRWELL (2,2);

    private final int width, height;

    GameObjectType(int w, int h)
    {
        this.width   = w;
        this.height  = h;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}