package io.fgame.base;

import com.badlogic.gdx.math.Vector2;
import io.fgame.base.tiles.TerrainType;

/**
 * Created by user for io.fgame.base at 20/07/14 19:47
 */

public class TerrainSaveObject{
    public TerrainType t;
    public boolean cMasterTile;
    public Vector2 cMasterTileLocation;
    public float oilLevel;
    public CityNames cName;
}
