package io.fgame.base;

import com.badlogic.gdx.graphics.Texture;
import io.fgame.base.tiles.TerrainTile;

/**
 * Created by user for io.fgame.base at 30/07/14 20:16
 */
public class OilWell extends GameObject {
    private int barrelsExtracted;
    private int wellRadius; //the maximum radius the well can pump from in tiles.

    public OilWell(Texture tex) {
        super(tex);
        wellRadius = 2;
    }

    public void Update(TerrainTile[][] t, int x, int y){
        //calculate oil levels around the well
        for (int x1 =  x -7; x1 < x + 7; x1++){
            for (int y1 = y - 7; y1 < y + 7; y1++){
                //3 tiles
                if (Math.pow(x1-x, 2) + Math.pow(y1-y,2) < Math.pow(wellRadius,2)){
                    //Calculate resource gains from these, inside the radius tiles
                    float oilLevel = t[x1][y1].getOilLevel();
                    oilLevel = oilLevel - (float)Math.random() / 10000;
                    t[x1][y1].setOilLevel(oilLevel);
                }

            }
        }

    }
}
