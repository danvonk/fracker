package io.fgame.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import io.fgame.base.tiles.CityTile;
import io.fgame.base.tiles.TerrainTile;
import io.fgame.base.tiles.TerrainType;

import javax.xml.soap.Text;


public class CityGenerator {
    Texture c2;
    private CityTile[][] ct;

    public CityGenerator() {

    }

    public static void fromTerrainSaveObject(TerrainSaveObject[][] saveObjects){
        Texture c2 = new Texture("rtext/c2.png");
        CityTile[][] ct = new CityTile[128][128];
        for (int x = 0; x < 128; x++){
            for (int y = 0; y < 128; y++){
                if (saveObjects[x][y].cMasterTileLocation != null){
                    ct[x][y] = new CityTile(c2);
                    ct[x][y].setMasterTile(saveObjects[x][y].cMasterTile);
                    if (saveObjects[x][y].cMasterTile){
                        ct[x][y].setMTile(new Vector2(x,y));

                    }
                }
            }
        }
    }

    public static CityTile[][] createCities(TerrainTile[][] tiles) {
        CityTile[][] ct = new CityTile[128][128];
        Texture c2 = new Texture("rtext/c1.png");
        //find a place to fit a city/town
        //circle with centre a,b: (x-a)^2 (y-b)^2 = r^2
        for (int x = 0; x < tiles.length; x++) {
            for (int y = 0; y < tiles[0].length; y++) {
                //TODO: Implement a quadtree to check if there are any cities nearby.
                //2L,2R,U,D
                if (x > 10 && y > 10 && x < 120 && y < 120) {
                    if (tiles[x][y].getTileType() == TerrainType.PLAINS && tiles[x - 1][y].getTileType() == TerrainType.PLAINS &&
                            tiles[x + 1][y].getTileType() == TerrainType.PLAINS && tiles[x][y + 1].getTileType() == TerrainType.PLAINS &&
                            tiles[x][y - 1].getTileType() == TerrainType.PLAINS && tiles[x - 2][y].getTileType() == TerrainType.PLAINS &&
                            tiles[x + 2][y].getTileType() == TerrainType.PLAINS) {
                        //TODO: Save a tad by cancelling the vicinity check on the first error.
                        int error = 0;
                        //6x6 around city
                        for (int x1 = x - 6; x1 < x + 6; x1++) {
                            for (int y1 = y - 6; y1 < y + 6; y1++) {
                                if (ct[x1][y1] != null) {
                                    error++;
                                }
                            }
                        }
                        if (error == 0) {
                            if (Math.random() < 0.05) {
                                ct[x][y] = new CityTile(c2);
                                ct[x][y].setMasterTile(true);
                                ct[x][y].setMTile(new Vector2(x,y));
                                ct[x][y].generateName();
                                ct[x][y].setPosition(x, y);
                                ct[x][y].setSize(1, 1);

                                //create some more aux. city tiles around it
                                for (int x1 = x - 7; x1 < x + 7; x1++) {
                                    for (int y1 = y - 7; y1 < y + 7; y1++) {
                                        if (Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2) < 12 && tiles[x1][y1].getTileType() != TerrainType.DEEP_WATER &&
                                                tiles[x1][y1].getTileType() != TerrainType.SHALLOW_WATER) {
                                            if (Math.random() < 0.45) {
                                                ct[x1][y1] = new CityTile(c2);
                                                ct[x1][y1].setMasterTile(false);
                                                ct[x1][y1].setPosition(x1, y1);
                                                ct[x1][y1].setMTile(new Vector2(x,y));
                                                ct[x1][y1].setSize(1, 1);
                                            }

                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            } //end tiles iteration

        }
        return ct;

    }
}
