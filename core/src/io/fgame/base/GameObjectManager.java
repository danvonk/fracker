package io.fgame.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by user for io.fgame.base at 05/08/14 19:07
 */
public class GameObjectManager {
    private List<GameObject> permGameObjects;

    public GameObjectManager(){
        permGameObjects = new LinkedList<GameObject>();
    }
    public void newGameObject(GameObject e, int x, int y){
        //find the g.o.s type
        if (e instanceof OilWell){
            OilWell oilWell = new OilWell(new Texture("ow2.png"));
            oilWell.setPosition(x,y);
            oilWell.setSize(1,1);
            permGameObjects.add(oilWell);
        }
    }
    public void render(SpriteBatch b){
        if (!permGameObjects.isEmpty()) {
            for (GameObject permGameObject : permGameObjects) {
                permGameObject.draw(b);
            }
        }
    }
    public void update(){
        for (GameObject permGameObject: permGameObjects){
            if (permGameObject instanceof OilWell){

            }

        }
    }
}
