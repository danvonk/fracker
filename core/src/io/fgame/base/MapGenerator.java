package io.fgame.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import io.fgame.base.tiles.TerrainTile;
import io.fgame.base.tiles.TerrainType;

/**
 * Created by dan on 15/07/14.
 */

public class MapGenerator {
    private static ResourceGenerator resourceGenerator;

    public MapGenerator() {

    }

    public static TerrainTile[][] generateMap(int w, int h) {
        resourceGenerator = new ResourceGenerator();
        final float k_dWater = 0.4f;
        final float k_sWater = 0.45f;
        final float k_desert = 0.5f;
        final float k_plains = 0.62f;
        final float k_grass = 0.7f;
        final float k_forest = 0.8f;
        final float k_hills = 0.88f;
        final float k_mountains = 0.9f;

        Texture deepWater = new Texture(Gdx.files.internal("rtext/dwater_t.png"));
        Texture shallowWater = new Texture(Gdx.files.internal("rtext/tex_Water.jpg"));
        Texture desert = new Texture(Gdx.files.internal("rtext/Sand_5_Diffuse.png"));
        Texture plains = new Texture(Gdx.files.internal("rtext/dry_grass.png"));
        Texture grass = new Texture(Gdx.files.internal("rtext/grass.png"));
        Texture hills = new Texture(Gdx.files.internal("rtext/hills.png"));
        Texture forest = new Texture(Gdx.files.internal("rtext/forest.png"));
        Texture mountains = new Texture(Gdx.files.internal("rtext/forest.png"));


        TerrainTile[][] terrain = new TerrainTile[w][h];

        float[][] baseNoise = PerlinNoise.generatePerlinNoise(w, h, 6);

        int[][] pNoise = new int[w][h];
        for (int z = 0; z < baseNoise.length; z++) {
            for (int x = 0; x < baseNoise[z].length; x++) {
                if (baseNoise[x][z] < k_dWater) pNoise[x][z] = 0;
                else if (baseNoise[x][z] < k_sWater) pNoise[x][z] = 1;
                else if (baseNoise[x][z] < k_desert) pNoise[x][z] = 2;
                else if (baseNoise[x][z] < k_plains) pNoise[x][z] = 3;
                else if (baseNoise[x][z] < k_grass) pNoise[x][z] = 4;
                else if (baseNoise[x][z] < k_forest) pNoise[x][z] = 5;
                else if (baseNoise[x][z] < k_hills) pNoise[x][z] = 6;
                else if (baseNoise[x][z] < k_mountains) pNoise[x][z] = 7;
                else if (baseNoise[x][z] > k_mountains) pNoise[x][z] = 7;
            }
        }

        for (int x = 0; x < pNoise.length; x++) {
            for (int y = 0; y < pNoise[x].length; y++) {
                switch (pNoise[x][y]) {
                    case 0:
                        terrain[x][y] = new TerrainTile(deepWater);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);
                        terrain[x][y].setTileType(TerrainType.DEEP_WATER);
                        break;
                    case 1:
                        terrain[x][y] = new TerrainTile(shallowWater);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.SHALLOW_WATER);
                        break;
                    case 2:
                        terrain[x][y] = new TerrainTile(desert);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.DESERT);
                        break;
                    case 3:
                        terrain[x][y] = new TerrainTile(plains);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.PLAINS);
                        break;
                    case 4:
                        terrain[x][y] = new TerrainTile(grass);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.GRASS);
                        break;
                    case 5:
                        terrain[x][y] = new TerrainTile(forest);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.FOREST);
                        break;
                    case 6:
                        terrain[x][y] = new TerrainTile(hills);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.HILLS);
                        break;
                    case 7:
                        terrain[x][y] = new TerrainTile(mountains);
                        terrain[x][y].setPosition(x, y);
                        terrain[x][y].setSize(1, 1);

                        terrain[x][y].setTileType(TerrainType.MOUNTAINS);
                        break;
                    default:
                        break;
                }
            }
        }

        resourceGenerator.createResources(terrain);
        /*dispose of these because apparently they are not removed when out of scope.
        deepWater.dispose();
        shallowWater.dispose();
        desert.dispose();
        plains.dispose();
        grass.dispose();
        forest.dispose();
        hills.dispose();
        mountains.dispose();
        */
        return terrain;
    }

    public static TerrainTile[][] fromTerrainSaveObject(TerrainSaveObject[][] s){
        Texture deepWater = new Texture(Gdx.files.internal("rtext/dwater_t.png"));
        Texture shallowWater = new Texture(Gdx.files.internal("rtext/tex_Water.jpg"));
        Texture desert = new Texture(Gdx.files.internal("rtext/Sand_5_Diffuse.png"));
        Texture plains = new Texture(Gdx.files.internal("rtext/dry_grass.png"));
        Texture grass = new Texture(Gdx.files.internal("rtext/grass.png"));
        Texture hills = new Texture(Gdx.files.internal("rtext/hills.png"));
        Texture forest = new Texture(Gdx.files.internal("rtext/forest.png"));
        Texture mountains = new Texture(Gdx.files.internal("rtext/forest.png"));


        TerrainTile[][] t = new TerrainTile[s[0].length][s.length];

        for (int x = 0; x < s.length; x++) {
            for (int y = 0; y < s.length; y++) {
                switch (s[x][y].t) {
                    case DEEP_WATER:
                        t[x][y] = new TerrainTile(deepWater);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                    case SHALLOW_WATER:
                        t[x][y] = new TerrainTile(shallowWater);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case DESERT:
                        t[x][y] = new TerrainTile(desert);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case PLAINS:
                        t[x][y] = new TerrainTile(plains);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case GRASS:
                        t[x][y] = new TerrainTile(grass);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case FOREST:
                        t[x][y] = new TerrainTile(forest);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case HILLS:
                        t[x][y] = new TerrainTile(hills);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                    case MOUNTAINS:
                        t[x][y] = new TerrainTile(mountains);
                        t[x][y].setOilLevel(s[x][y].oilLevel);
                        t[x][y].setTileType(s[x][y].t);
                        t[x][y].setPosition(x, y);
                        t[x][y].setSize(1, 1);
                        break;
                }
            }
        }

        return t;

    }

}
