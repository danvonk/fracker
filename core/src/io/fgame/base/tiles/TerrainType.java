package io.fgame.base.tiles;

/**
 * Created by dan on 18/07/14.
 */

public enum TerrainType {
    DEEP_WATER,
    SHALLOW_WATER,
    DESERT,
    PLAINS,
    GRASS,
    FOREST,
    HILLS,
    MOUNTAINS
}
