package io.fgame.base.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import io.fgame.base.CityNames;

import java.util.Random;

/**
 * Created by user on 19/07/14.
 */
public class CityTile extends Sprite {
    private boolean isMasterTile;
    private Vector2 masterTile; //the centre of the city
    private CityNames cityName;

    public CityTile(Texture tex) {
        super(tex, 0, 0, tex.getWidth(), tex.getHeight());
    }

    public void generateName() {
        Random random = new Random();
        cityName = CityNames.values()[random.nextInt(CityNames.values().length)];
        Gdx.app.log("FRGame", "City Created with name " + cityName.name());
    }

    public void setMTile(Vector2 t){
        this.masterTile = t;
    }

    public Vector2 getMTile(){
        return this.masterTile;
    }


    public boolean isMasterTile() {
        return isMasterTile;
    }

    public void setMasterTile(boolean isMasterTile) {
        this.isMasterTile = isMasterTile;
    }

    public CityNames getCityName(){
        return this.cityName;
    }

    public void setCityName(CityNames c){
        this.cityName = c;
    }
}
