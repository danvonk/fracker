package io.fgame.base.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;


public class TerrainTile extends Sprite {
    private TerrainType tileType;
    private float oilLevel;

    public TerrainTile() {
        oilLevel = 0;
    }

    public TerrainTile(Texture tex) {
        super(tex, 0, 0, tex.getWidth(), tex.getHeight());
    }


    public TerrainType getTileType() {
        return tileType;
    }

    public void setTileType(TerrainType tileType) {
        this.tileType = tileType;
    }

    public float getOilLevel() {
        return oilLevel;
    }

    public void setOilLevel(float oilLevel) {
        this.oilLevel = oilLevel;
    }
}
