package io.fgame;

import com.badlogic.gdx.Game;
import io.fgame.screens.Fracker;

public class FRGame extends Game{

    @Override
    public void create () {
        Fracker fracker = new Fracker();
        setScreen(fracker);

    }

    @Override
    public void render () {
        super.render();

    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void resume(){
        super.resume();

    }

    @Override
    public void pause(){
        super.pause();
    }

    @Override
    public void resize(int x, int y){
        super.resize(x,y);

    }


}